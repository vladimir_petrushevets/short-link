<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShortLink extends Model {

	protected $table = 'links';

	protected $fillable = [
		'code',
		'link',
		'limit',
		'time'
	];
}