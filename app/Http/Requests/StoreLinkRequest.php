<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreLinkRequest extends FormRequest {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [
			'link'  => 'required|url',
			'limit' => 'required|integer|min:0',
			'time'  => 'required|integer|max:86400|min:0'
		];
	}

	/**
	 * Get error messages for specific validation rules.
	 *
	 * @return array
	 */
	public function messages() {
		return [
			'link.required'  => 'Ссылка обязательное поле',
			'limit.required' => 'Лимит обязательное поле',
			'link.url'       => 'Ссылка должна быть url адресом',
			'limit.integer'  => 'Лимит должен быть целым числом',
			'limit.min'      => 'Лимит должен быть положительным числом',
			'time.required'  => 'Время жизни обязательное поле',
			'time.integer'   => 'Время жизни должно быть целым числом',
			'time.max'       => 'Максимальное время жизни 86400 секунд',
			'time.min'      => 'Время жизни должно быть положительным числом',
		];
	}
}
