<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ShortLink;
use Illuminate\Support\Str;
use App\Http\Requests\StoreLinkRequest;

class ShortLinkController extends Controller {

	public function index() {
		return view( 'shortenLink', [
			'links' => ShortLink::latest()->get()
		] );
	}

	/**
	 * Store a new link.
	 *
	 * @param  \App\Http\Requests\StoreLinkRequest $request
	 *
	 * @return Response
	 */
	public function store( StoreLinkRequest $request ) {

		ShortLink::create( [
			'link'  => $request->link,
			'code'  => str_random( 8 ),
			'limit' => $request->limit,
			'time'  => $request->time
		] );

		return redirect( '/' )->with( 'success', 'Короткая ссылка успешно создана!' );
	}

	/**
	 * Makes a redirect on link
	 *
	 * @param string $code
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function shortenLink( $code ) {
		$link = ShortLink::where( 'code', $code )->first();

		$link->count ++;
		$link->save();

		$this->checkLimit( $link );
		$this->checkTime( $link );

		return redirect( $link->link );
	}

	/**
	 * Checks the reference for the usage count
	 *
	 * @param object $model
	 *
	 * @return mixed
	 */
	protected function checkLimit( $model ) {
		if ( $model->limit != 0 && $model->count > $model->limit ) {
			return abort( 404 );
		}

		return true;
	}

	/**
	 * Checks the lifetime of the link
	 *
	 * @param $model
	 *
	 * @return mixed
	 */
	protected function checkTime( $model ) {
		$timeLive = time() - strtotime( $model->created_at );

		if ( $timeLive >= $model->time ) {
			return abort( 404 );
		}

		return true;
	}
}